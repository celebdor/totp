totp
====

Command line utility to generate pin + totp combinations

Non-python requirements
-----------------------

* *Unix pass*: Where the token url and pin are stored and **totp** will retrieve
  them from.

Functionality
-------------

* generate: shows pin + totp combination

The totp url and pin are to be stored in *unix pass* with a PATH stored in an
environment variable:

    TOKEN_PASS_PATH

The path must have two items:

* url of the form: *otpauth://totp/xxx?secret=GG*
* pin: string that is to be prepended to the generated otp

For example, one could store it in:

    tokens/servers/mycooltoken/url
    tokens/servers/mycooltoken/pin
