# -*- coding: utf-8 -*-
# Copyright 2016 Antoni Segura Puimedon <celebdor@gmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import logging
import subprocess


LOG = logging.getLogger()


def get_pass_secret(pass_path):
    LOG.info('Getting pass secret...')
    s = subprocess.Popen(['pass',
                          'show',
                          pass_path],
                         stdin=subprocess.PIPE,
                         stdout=subprocess.PIPE,
                         stderr=subprocess.PIPE,
                         universal_newlines=True)

    outs, errs = s.communicate()

    if not errs:
        secret = outs.rstrip()
    else:
        raise ValueError('Failed to retrieve unix pass secret. Err: {}'.format(
            errs))

    return secret
