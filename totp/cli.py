# -*- coding: utf-8 -*-
# Copyright 2016 Antoni Segura Puimedon <celebdor@gmail.com>
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging
from oath import GoogleAuthenticator
import os
import sys

import click
import pyperclip

from totp import utils

LOG = logging.getLogger()


@click.group()
def cli():
    pass


@click.command(help='Generates the auth combination')
@click.option('--clipboard', '-c',
              is_flag=True,
              help='Do not print, copy clipboard')
@click.option('--keyboard', '-k',
              is_flag=True,
              help='Do not print, nor copy simulate keyboard')
def generate(clipboard, keyboard):
    pass_path = os.environ.get('TOKEN_PASS_PATH')
    if pass_path is None:
        LOG.error('TOKEN_PASS_PATH environment variable must be defined')
        sys.exit(1)
    token_url = utils.get_pass_secret('{}/url'.format(pass_path))
    token_pin = utils.get_pass_secret('{}/pin'.format(pass_path))

    otp = GoogleAuthenticator(token_url)
    auth_text = '{}{}'.format(token_pin, otp.generate())
    if keyboard:
        import pyautogui
        pyautogui.typewrite(auth_text)
    elif clipboard:
        pyperclip.copy(auth_text)
    else:
        print(auth_text)


cli.add_command(generate)


def main():
    handler = logging.StreamHandler(sys.stderr)
    LOG.setLevel(logging.INFO)
    LOG.addHandler(handler)
    cli()
